#!/usr/bin/env python3

# job name
#PBS -N gizmo_track
# node configuration
# select = nodes; ncpus = cpus per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Ivy Bridge node: 20 cores, ~60 GB
##PBS -l select=1:ncpus=20:mpiprocs=1:ompthreads=12:model=ivy
# Haswell node: 24 cores, 128 GB
#PBS -l select=1:ncpus=24:mpiprocs=1:ompthreads=1:model=has
## Broadwell node: 28 cores, 128 GB
##PBS -l select=1:ncpus=28:mpiprocs=1:ompthreads=5:model=bro
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
##PBS -q devel
##PBS -q normal
#PBS -q long
#PBS -l walltime=24:00:00
# combine stderr and stdout into one file
#PBS -j oe
# output file name
##PBS -o track/gizmo_track_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -M arwetzel@gmail.com
#PBS -m ae
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit job to queue to track particles across time in Gizmo snapshots.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_track
from gizmo_analysis import gizmo_default

# ensure that output track directory exists
os.makedirs(gizmo_default.track_directory, exist_ok=True)

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('pbs', 1, 1)

# parameters
function_kind = 'pointer+coordinate'  # assign particle pointer indices and host coordinates
species_names = ['star', 'gas']  # to both gas and stars
snapshot_indices = 'all'  # snapshots to generate pointers to
reference_snapshot_index = 'final'  # which snapshot to use as reference (typically z = 0)
host_number = 1  # how many hosts to assign star formation coordinates relative to

# parse input arguments
if len(os.sys.argv) > 1:
    function_kind = str(os.sys.argv[1])
    assert ['pointer' in function_kind or 'coordinate' in function_kind]

if len(os.sys.argv) > 2:
    snapshot_index_min = int(os.sys.argv[2])
    if len(os.sys.argv) > 3:
        snapshot_index_max = int(os.sys.argv[3])
    snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)

if len(os.sys.argv) > 4:
    assert 'coordinate' in function_kind
    host_number = int(os.sys.argv[4])
    assert 1 <= host_number <= 2

print_string = ''
if 'pointer' in function_kind:
    print_string += f'* assigning {function_kind}s to {species_names} particles\n'
    print_string += f'* at snapshots:\n{snapshot_indices}'
if 'coordinate' in function_kind:
    print_string += f'* assigning {host_number} host[s] (coordinates, rotation tensor)\n'
    print_string += '  and star formation coordinates wrt host[s]\n'
print(print_string)

os.sys.stdout.flush()

# execute
if 'pointer' in function_kind:
    ParticlePointer = gizmo_track.ParticlePointerClass(species_names)
    ParticlePointer.generate_write_pointers(
        snapshot_indices=snapshot_indices,
        reference_snapshot_index=reference_snapshot_index,
        proc_number=ScriptPrint.omp_number
    )

if 'coordinate' in function_kind:
    ParticleCoordinate = gizmo_track.ParticleCoordinateClass('star')
    ParticleCoordinate.generate_write_hosts_coordinates(
        host_number=host_number, reference_snapshot_index=reference_snapshot_index
    )

# print run-time information
ScriptPrint.print_runtime()
