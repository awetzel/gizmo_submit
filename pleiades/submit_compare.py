#!/usr/bin/env python3

# job name
#PBS -N gizmo_compare
# node configuration
# select = nodes; ncpus = cpus per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Ivy Bridge node: 20 cores, ~60 GB
##PBS -l select=1:ncpus=20:mpiprocs=1:ompthreads=12:model=ivy
# Haswell node: 24 cores, 128 GB
#PBS -l select=1:ncpus=24:mpiprocs=1:ompthreads=1:model=has
## Broadwell node: 28 cores, 128 GB
##PBS -l select=1:ncpus=28:mpiprocs=1:ompthreads=5:model=bro
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
##PBS -q devel
#PBS -q normal
##PBS -q long
#PBS -l walltime=8:00:00
# combine stderr and stdout into one file
#PBS -j oe
# output file name
##PBS -o gizmo_compare_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -M arwetzel@gmail.com
#PBS -m ae
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit comparison of different simulations.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

from utilities import io as ut_io
from gizmo_analysis import gizmo_plot

simulation_directories = {
    'm12i_res880': 'm12i r880',
    'm12i_res7100': 'm12i r7k',
    'm12i_res7100_cr-heat-fix': 'm12i r7k cr-fix',
    'm12i_res7100_nmd': 'm12i r7k nmd',
    #   'm12i_res7100_uvb-late', 'm12i rkl uvb-late'],
    'm12i_res57000': 'm12i r57k',
}

species = ['star', 'gas', 'dark']  # which particle species to analyze

redshifts = [0, 0.4, 0.5, 0.6, 0.8, 1]

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# read snapshots and analyze
CompareSimulations = gizmo_plot.CompareSimulationsClass()
CompareSimulations.plot(
    species=species, simulation_directories=simulation_directories, redshifts=redshifts
)

# print run-time information
ScriptPrint.print_runtime()
