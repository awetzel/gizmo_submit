#!/usr/bin/env python3

# job name
#PBS -N gizmo_compress
# node configuration
# select = nodes; ncpus = cpus per node; mpiprocs, ompthreads = mpi tasks, openmp threads per node
# Ivy Bridge node: 20 cores, ~60 GB
##PBS -l select=1:ncpus=20:mpiprocs=1:ompthreads=12:model=ivy
# Haswell node: 24 cores, 128 GB
#PBS -l select=1:ncpus=24:mpiprocs=1:ompthreads=1:model=has
## Broadwell node: 28 cores, 128 GB
##PBS -l select=1:ncpus=28:mpiprocs=1:ompthreads=5:model=bro
# queue: devel <= 2 hr, normal <= 8 hr, long <= 5 day
##PBS -q devel
##PBS -q normal
#PBS -q long
#PBS -l walltime=24:00:00
# combine stderr and stdout into one file
#PBS -j oe
# output file name
##PBS -o gizmo_jobs/gizmo_compress_job_$PBS_JOBID.txt
# email results: a = aborted, b = begin, e = end
#PBS -M arwetzel@gmail.com
#PBS -m ae
# import terminal environmental variables
#PBS -V
# account to charge
#PBS -W group_list=s2355
##PBS -W group_list=s2445
##PBS -W group_list=s2566

'''
Submit compression of gizmo snapshot files to queue.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_file

# parameters
snapshot_index_min = 0
snapshot_index_max = 600
write_directory = 'output_comp'

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('pbs', 1, 1)

# parse input arguments
if len(os.sys.argv) > 1:
    snapshot_index_min = int(os.sys.argv[1])
    if len(os.sys.argv) > 2:
        snapshot_index_max = int(os.sys.argv[2])
snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)

# execute
Compress = gizmo_file.CompressClass()

Compress.compress_snapshots(
    snapshot_indices=snapshot_indices,
    write_directory=write_directory,
    proc_number=ScriptPrint.omp_number,
)

Compress.test_compression(snapshot_directory=write_directory)

# print run-time information
ScriptPrint.print_runtime()
