#!/usr/bin/env python3

#SBATCH --job-name=gizmo_compare
#SBATCH --partition=high2    # peloton regular node: 32 cores, 8 GB per core, 256 GB total
##SBATCH --partition=high2m    # peloton high-mem node: 32 cores, 16 GB per core, 512 GB total
#SBATCH --mem=125G
##SBATCH --nodes=1
#SBATCH --ntasks=1    # MPI tasks total
##SBATCH --ntasks-per-node=1    # MPI tasks per node (does not work on peloton)
#SBATCH --cpus-per-task=16    # OpenMP threads per MPI task
#SBATCH --time=12:00:00
#SBATCH --output=gizmo_compare_job_%j.txt
#SBATCH --mail-user=arwetzel@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end

'''
Submit comparison of different simulations.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

from utilities import io as ut_io
from gizmo_analysis import gizmo_plot

simulation_directories = {
    'm12i_res880': 'm12i r880',
    'm12i_res7100': 'm12i r7k',
    'm12i_res7100_cr-heat-fix': 'm12i r7k cr-fix',
    'm12i_res7100_nmd': 'm12i r7k nmd',
    #   'm12i_res7100_uvb-late', 'm12i rkl uvb-late'],
    'm12i_res57000': 'm12i r57k',
}

species = ['star', 'gas', 'dark']  # which particle species to analyze

redshifts = [0, 0.4, 0.5, 0.6, 0.8, 1]

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# read snapshots and analyze
gizmo_plot.CompareSimulations.plot(
    species=species, simulation_directories=simulation_directories, redshifts=redshifts
)

# print run-time information
ScriptPrint.print_runtime()
