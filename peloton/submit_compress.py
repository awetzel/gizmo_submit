#!/usr/bin/env python3

#SBATCH --job-name=gizmo_compress
#SBATCH --partition=high2    # peloton regular node: 32 cores, 8 GB per core, 256 GB total
##SBATCH --partition=high2m    # peloton high-mem node: 32 cores, 16 GB per core, 512 GB total
##SBATCH --mem=250G
#SBATCH --nodes=1
#SBATCH --ntasks=1    # MPI tasks total
##SBATCH --ntasks-per-node=1    # MPI tasks per node (does not work on peloton)
#SBATCH --cpus-per-task=8    # OpenMP threads per MPI task
#SBATCH --time=12:00:00
#SBATCH --output=gizmo_jobs/gizmo_compress_job_%j.txt
#SBATCH --mail-user=arwetzel@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end

'''
Submit compression of gizmo snapshot files to queue.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_file

# parameters
snapshot_indices = None  # None = run on all snapshots
write_directory = 'output_comp'

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# parse input arguments
if len(os.sys.argv) > 1:
    snapshot_index_min = int(os.sys.argv[1])
    snapshot_index_max = 600
    if len(os.sys.argv) > 2:
        snapshot_index_max = int(os.sys.argv[2])
    snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)

# execute
Compress = gizmo_file.CompressClass()

Compress.compress_snapshots(
    snapshot_indices=snapshot_indices,
    write_directory=write_directory,
    proc_number=ScriptPrint.omp_number,
)

Compress.test_compression(snapshot_directory=write_directory)

# print run-time information
ScriptPrint.print_runtime()
