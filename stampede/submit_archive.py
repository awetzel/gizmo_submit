#!/usr/bin/env python3.11

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=gizmo_archive
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=20    # OpenMP threads per MPI task
#SBATCH --time=12:00:00
#SBATCH --output=gizmo_archive_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-PHY240075

'''
Submit to queue a job to clean + archive Gizmo simulation directory[s].
Submit this script via the following options:
    (1) within the base directory of a simulation
    (2) within a directory that contains multiple simulation directories, to run on each one
    (3) set directories to run on specific simulation directories

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

from utilities import io as ut_io
from gizmo_analysis import gizmo_file
#import glob

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# directory[s] to run on
directories = '.'
#directories = []
#directories = glob.glob('m12*')  # need to import glob too
#directories.sort()

# whether to delete the directories after tar-ing them into a single file
delete_directories = False

# execute
gizmo_file.Archive.clean_directories(directories)

gizmo_file.Archive.tar_directories(
    directories, delete_directories=delete_directories, proc_number=ScriptPrint.omp_number,
)

# print run-time information
ScriptPrint.print_runtime()
