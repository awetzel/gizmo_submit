#!/usr/bin/env python3.11

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=gizmo_compress
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=6    # OpenMP threads per MPI task
#SBATCH --time=8:00:00
#SBATCH --output=gizmo_jobs/gizmo_compress_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-PHY240075

'''
Submit compression of gizmo snapshot files to queue.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_file

# parameters
snapshot_indices = None  # None = run on all snapshots
write_directory = 'output_comp'
python_executable = 'python3.10'

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# parse input arguments
if len(os.sys.argv) > 1:
    snapshot_index_min = int(os.sys.argv[1])
    snapshot_index_max = 600
    if len(os.sys.argv) > 2:
        snapshot_index_max = int(os.sys.argv[2])
    snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)

# execute
Compress = gizmo_file.CompressClass(python_executable=python_executable)

Compress.compress_snapshots(
    snapshot_indices=snapshot_indices,
    write_directory=write_directory,
    proc_number=ScriptPrint.omp_number,
)

Compress.test_compression(snapshot_directory=write_directory)

# print run-time information
ScriptPrint.print_runtime()
