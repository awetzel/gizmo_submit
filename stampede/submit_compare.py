#!/usr/bin/env python3.11

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=gizmo_compare
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=2:00:00
#SBATCH --output=gizmo_compare_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-PHY240075

'''
Submit comparison of different simulations.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

from utilities import io as ut_io
from gizmo_analysis import gizmo_plot

simulation_directories = {
    'm12i_res880': 'm12i r880',
    'm12i_res7100': 'm12i r7k',
    'm12i_res7100_cr-heat-fix': 'm12i r7k cr-fix',
    'm12i_res7100_nmd': 'm12i r7k nmd',
    #   'm12i_res7100_uvb-late', 'm12i rkl uvb-late'],
    'm12i_res57000': 'm12i r57k',
}

species = ['star', 'gas', 'dark']  # which particle species to analyze

redshifts = [0, 0.4, 0.5, 0.6, 0.8, 1]

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# read snapshots and analyze
gizmo_plot.CompareSimulations.plot(
    species=species, simulation_directories=simulation_directories, redshifts=redshifts
)

# print run-time information
ScriptPrint.print_runtime()
