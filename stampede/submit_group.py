#!/usr/bin/env python3.11

# Stampede3
# SPR node: 112 cores, 128 GB
# ICX node: 80 cores, 256 GB, 3.2 (3.0 useable) GB per core
# SKX node: 48 cores, 192 GB, 4.0 (3.5 useable) GB per core
#SBATCH --job-name=gizmo_group
#SBATCH --partition=icx
##SBATCH --partition=skx
##SBATCH --partition=skx-dev
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=8    # OpenMP threads per MPI task
#SBATCH --time=2:00:00
#SBATCH --output=group/group_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=TG-PHY240075

'''
Submit FoF group finding to queue.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_group
from gizmo_analysis import gizmo_default

# ensure that output directory exists
os.makedirs(gizmo_default.group_directory, exist_ok=True)

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# default parameters
species_name = 'gas'
snapshot_index_min = 1
snapshot_index_max = 600

if species_name == 'gas':
    linking_length = 24  # [pc physical]
    particle_number_min = 10  # to keep a group
    property_select = {
        'number.density': [10, np.inf],
        'temperature': [0, 1e4],
    }  # properties to select on

elif species_name == 'star':
    linking_length = 8  # [pc physical]
    particle_number_min = 5  # to keep a group
    property_select = {'age': [0, 0.022]}  # properties to select on

# parse input arguments
if len(os.sys.argv) > 1:
    species_name = str(os.sys.argv[1])
assert species_name in ['gas', 'star']

if len(os.sys.argv) > 2:
    snapshot_index_min = int(os.sys.argv[2])
    if len(os.sys.argv) > 3:
        snapshot_index_max = int(os.sys.argv[3])

if len(os.sys.argv) > 4:
    linking_length = int(os.sys.argv[4])
assert linking_length > 0

if len(os.sys.argv) > 5:
    particle_number_min = int(os.sys.argv[5])
assert particle_number_min > 1

snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)[::-1]

print(f'generating FoF groups using:')
print(f'* {species_name} particles')
print(f'* linking_length = {linking_length}')
print(f'* particle_number_min = {particle_number_min}')
print(f'* property_select = {property_select}')
print(f'at snapshots:\n{snapshot_indices}')
os.sys.stdout.flush()

# execute
IO = gizmo_group.IOClass()

IO.generate_write_group_catalogs(
    species_name,
    linking_length,
    particle_number_min,
    property_select,
    'index',
    snapshot_indices,
    proc_number=ScriptPrint.omp_number,
)

# print run-time information
ScriptPrint.print_runtime()
