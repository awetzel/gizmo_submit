#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=gizmo_compare
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=4:00:00
#SBATCH --output=gizmo_compare_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit comparison of different simulations.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

from utilities import io as ut_io
from gizmo_analysis import gizmo_plot

simulation_directories = {
    'm12i_res880': 'm12i r880',
    'm12i_res7100': 'm12i r7k',
    'm12i_res7100_cr-heat-fix': 'm12i r7k cr-fix',
    'm12i_res7100_nmd': 'm12i r7k nmd',
    #   'm12i_res7100_uvb-late', 'm12i rkl uvb-late'],
    'm12i_res57000': 'm12i r57k',
}

species = ['star', 'gas', 'dark']  # which particle species to analyze

redshifts = [0, 0.4, 0.5, 0.6, 0.8, 1]

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# read snapshots and analyze
gizmo_plot.CompareSimulations.plot(
    species=species, simulation_directories=simulation_directories, redshifts=redshifts
)

# print run-time information
ScriptPrint.print_runtime()
