#!/usr/bin/env python3

# Frontera node: 56 cores, 3.4 GB per core, 192 GB total
#SBATCH --job-name=gizmo_track
##SBATCH --partition=development  # 2 hours, 1-40 nodes, 1 job
#SBATCH --partition=small  # 2 days, 2-24 nodes, 20 jobs
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=48:00:00
#SBATCH --output=track/gizmo_track_job_%j.txt
#SBATCH --mail-user=awetzel@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --account=AST21010

'''
Submit job to queue to track particles across time in Gizmo snapshots.
Submit this script from within the base directory of the simulation.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
import numpy as np

from utilities import io as ut_io
from gizmo_analysis import gizmo_track
from gizmo_analysis import gizmo_default

# ensure that output directory exists
os.makedirs(gizmo_default.track_directory, exist_ok=True)

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')

# default parameters
function_kind = 'pointer'  # assign particle pointer indices
species_names = ['star', 'gas']  # to both gas and stars
snapshot_indices = 'all'  # which snapshots to generate pointers to
reference_snapshot_index = 'final'  # use final snapshot as reference (generally z = 0)
host_number = 1  # how many hosts to assign star formation coordinates relative to

# check if any input arguments
if len(os.sys.argv) > 1:
    function_kind = str(os.sys.argv[1])
    assert ['pointer' in function_kind or 'coordinate' in function_kind]

if len(os.sys.argv) > 2:
    snapshot_index_min = int(os.sys.argv[2])
    if len(os.sys.argv) > 3:
        snapshot_index_max = int(os.sys.argv[3])
    snapshot_indices = np.arange(snapshot_index_min, snapshot_index_max + 1)

if len(os.sys.argv) > 4:
    assert 'coordinate' in function_kind
    host_number = int(os.sys.argv[4])
    assert 1 <= host_number <= 2

print_string = ''
if 'pointer' in function_kind:
    print_string += f'* assigning {function_kind}s to {species_names} particles\n'
    print_string += f'* at snapshots:\n{snapshot_indices}'
if 'coordinate' in function_kind:
    print_string += f'* assigning {host_number} host[s] (coordinates, rotation tensor)\n'
    print_string += '  and star formation coordinates wrt host[s]\n'
print(print_string)

os.sys.stdout.flush()

# execute
if 'pointer' in function_kind:
    ParticlePointer = gizmo_track.ParticlePointerClass(species_names)
    ParticlePointer.generate_write_pointers(
        snapshot_indices=snapshot_indices,
        reference_snapshot_index=reference_snapshot_index,
        proc_number=ScriptPrint.omp_number,
    )

if 'coordinate' in function_kind:
    ParticleCoordinate = gizmo_track.ParticleCoordinateClass('star')
    ParticleCoordinate.generate_write_hosts_coordinates(
        host_number=host_number, reference_snapshot_index=reference_snapshot_index
    )

# print run-time information
ScriptPrint.print_runtime()
